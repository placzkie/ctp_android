# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Users\Leszekp\AppData\Local\Android\Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keep class pl.edu.agh.placzkie.ctp_supervisor.** { *; }
-keep interface pl.edu.agh.placzkie.ctp_supervisor.** { *; }

-dontwarn com.google.common.collect.MinMaxPriorityQueue

-keep class com.google.code.gson.** { *; }
-keep interface com.google.code.gson.** { *; }

-keep class com.google.gson.** { *; }
-keep interface com.google.gson.** { *; }

-keep class com.google.common.** { *; }
-keep interface com.google.common.** { *; }

-keep class com.google.guava.** { *; }
-keep interface com.google.guava.** { *; }

-keep class org.greenrobot.** { *; }
-keep interface org.greenrobot.** { *; }

-dontwarn javax.annotation.**
-keep class javax.annotation.** { *; }
-keep interface javax.annotation.** { *; }

-dontwarn javax.inject.**
-keep class javax.inject.** { *; }
-keep interface javax.inject.** { *; }

-dontwarn sun.misc.Unsafe

-keep class android.support.** { *; }
-keep interface android.support.** { *; }

-dontwarn android.support.v7.**
-keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }

-dontwarn android.support.v4.**
-keep class android.support.v4.** { *; }
-keep interface android.support.v4.** { *; }

-dontwarn android.support.design.**
-keep class android.support.design.** { *; }
-keep interface android.support.design.** { *; }
-keep public class android.support.design.R$* { *; }

-keep public class android.support.v7.widget.** { *; }
-keep public class android.support.v7.internal.widget.** { *; }
-keep public class android.support.v7.internal.view.menu.** { *; }

-dontwarn org.java_websocket.**
-keep class org.java_websocket.** { *; }
-keep interface org.java_websocket.** { *; }

-assumenosideeffects class android.util.Log {
    public static boolean isLoggable(java.lang.String, int);
    public static int v(...);
    public static int i(...);
    public static int w(...);
    public static int d(...);
    public static int e(...);
}