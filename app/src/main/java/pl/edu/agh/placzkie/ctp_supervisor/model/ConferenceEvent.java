package pl.edu.agh.placzkie.ctp_supervisor.model;

/**
 * Created by Leszek Placzkiewicz on 2016-09-22.
 */
public class ConferenceEvent {

    private Conference conference;

    public ConferenceEvent(Conference conference) {
        this.conference = conference;
    }

    public Conference getConference() {
        return conference;
    }
}
