package pl.edu.agh.placzkie.ctp_supervisor.model;

/**
 * Created by Leszek Placzkiewicz on 2016-10-30.
 */
public class ConferenceInfo {

    private final String conferenceName;

    private final String activeStartTime;

    private final String activeEndTime;

    public ConferenceInfo(String conferenceName, String activeStartTime, String activeEndTime) {
        this.conferenceName = conferenceName;
        this.activeStartTime = activeStartTime;
        this.activeEndTime = activeEndTime;
    }

    public String getConferenceName() {
        return conferenceName;
    }

    public String getActiveStartTime() {
        return activeStartTime;
    }

    public String getActiveEndTime() {
        return activeEndTime;
    }
}
