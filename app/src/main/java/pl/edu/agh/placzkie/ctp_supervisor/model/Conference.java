package pl.edu.agh.placzkie.ctp_supervisor.model;

/**
 * Created by Leszek Placzkiewicz on 2016-09-20.
 */
public class Conference {

    private boolean automaticLectureModeEnabled;

    private String cpLayout;
//    private final int currentLayout;

//    private final String layoutSource;

    //TODO - maybe use enum
    private FocusType focusType;

    private String focusParticipant;

    public static Conference createDefault() {
//        return new Conference(FocusType.VOICE_ACTIVATED, null, 1, "familyx", true);
        return new Conference(FocusType.VOICE_ACTIVATED, null, "family1", true);
    }

    public Conference(FocusType focusType,
                      String focusParticipant,
                      String cpLayout,
//                      int currentLayout,
//                      String layoutSource,
                      boolean automaticLectureModeEnabled) {
        this.focusType = focusType;
        this.focusParticipant = focusParticipant;
//        this.currentLayout = currentLayout;
//        this.layoutSource = layoutSource;
        this.cpLayout = cpLayout;
        this.automaticLectureModeEnabled = automaticLectureModeEnabled;
    }

    public void setAutomaticLectureModeEnabled(boolean automaticLectureModeEnabled) {
        this.automaticLectureModeEnabled = automaticLectureModeEnabled;
    }

    public boolean isAutomaticLectureModeEnabled() {
        return automaticLectureModeEnabled;
    }

    public void setCpLayout(String cpLayout) {
        this.cpLayout = cpLayout;
    }

    public FocusType getFocusType() {
        return focusType;
    }

    public String getFocusParticipant() {
        return focusParticipant;
    }

    public String getCpLayout() {
        return cpLayout;
    }

    public void setFocusType(FocusType focusType) {
        this.focusType = focusType;
    }

    public void setFocusParticipant(String focusParticipant) {
        this.focusParticipant = focusParticipant;
    }

    //    public int getCurrentLayout() {
//        return currentLayout;
//    }
//
//    public String getLayoutSource() {
//        return layoutSource;
//    }
}
