package pl.edu.agh.placzkie.ctp_supervisor.activities.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import pl.edu.agh.placzkie.ctp_supervisor.R;

/**
 * Created by Leszek Placzkiewicz on 2016-10-10.
 */
public class ConferencesAdapter extends RecyclerView.Adapter<ConferencesAdapter.ConferenceViewHolder>{
    private static final String TAG = ConferencesAdapter.class.getSimpleName();

    private ConferenceViewHolder.ConferenceClickListener clickListener;

    private List<String> conferencesList;


    public ConferencesAdapter(List<String> conferencesList,
                              ConferenceViewHolder.ConferenceClickListener clickListener) {
        super();

        this.clickListener = clickListener;
        this.conferencesList = conferencesList;
    }

    @Override
    public ConferenceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.conference_list_row, parent, false);

        return new ConferenceViewHolder(itemView, clickListener);
    }

    @Override
    public void onBindViewHolder(ConferenceViewHolder holder, int position) {
        String conference = conferencesList.get(position);

        holder.conferenceName.setText(conference);
    }

    @Override
    public int getItemCount() {
        return conferencesList.size();
    }


    public static class ConferenceViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private final ConferenceViewHolder.ConferenceClickListener clickListener;

        private TextView conferenceName;

        public ConferenceViewHolder(View itemView,
                                    final ConferenceViewHolder.ConferenceClickListener listener) {
            super(itemView);

            conferenceName = (TextView) itemView.findViewById(R.id.conference_name);

            clickListener = listener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(clickListener != null) {
                clickListener.onItemClicked(getLayoutPosition());
            }
        }

        public interface ConferenceClickListener {
            void onItemClicked(int position);
        }
    }
}
