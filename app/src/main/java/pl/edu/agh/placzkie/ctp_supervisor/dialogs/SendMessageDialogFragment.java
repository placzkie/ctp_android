package pl.edu.agh.placzkie.ctp_supervisor.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;

import pl.edu.agh.placzkie.ctp_supervisor.R;

/**
 * Created by Leszek Placzkiewicz on 2016-09-01.
 */
public class SendMessageDialogFragment extends DialogFragment {

    private String message;

    private SendMessageDialogListener mListener;


    public String getMessage() {
        return message;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (SendMessageDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement SendMessageDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_send_message, null);

        ArrayList<String> participantsNames = getArguments().getStringArrayList("participantsNames");

        builder.setView(view)
                .setMessage("Send message to: " + TextUtils.join(", ", participantsNames))
                .setPositiveButton(R.string.send, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        EditText editText = (EditText) view.findViewById(R.id.message);
                        message = editText.getText().toString();
                        mListener.onDialogPositiveClick(SendMessageDialogFragment.this);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogNegativeClick(SendMessageDialogFragment.this);
                    }
                });

        return builder.create();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        final Activity activity = getActivity();
        if (activity instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) activity).onDismiss(dialog);
        }
    }

    public interface SendMessageDialogListener {
        void onDialogPositiveClick(SendMessageDialogFragment dialog);
        void onDialogNegativeClick(SendMessageDialogFragment dialog);
    }
}
