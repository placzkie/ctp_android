package pl.edu.agh.placzkie.ctp_supervisor.model;

/**
 * Created by Leszek Placzkiewicz on 2016-09-20.
 */
public enum FocusType {
    VOICE_ACTIVATED("Voice activated"),
    PARTICIPANT("Participant");
//    H239;

    private final String description;

    FocusType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
