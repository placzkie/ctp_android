package pl.edu.agh.placzkie.ctp_supervisor.activities.additions;

/**
 * Created by Leszek Placzkiewicz on 2016-09-15.
 */
public class IntentConstants {

    public static final String USERNAME = "username";

    public static final String CONFERENCE = "conference";
}
