package pl.edu.agh.placzkie.ctp_supervisor.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;

import pl.edu.agh.placzkie.ctp_supervisor.R;

/**
 * Created by Leszek Placzkiewicz on 2016-09-03.
 */
public class ControlCameraDialogFragment extends DialogFragment {

    private ControlCameraDialogListener mListener;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (ControlCameraDialogListener) activity;
        } catch (ClassCastException ex) {
            throw new ClassCastException(activity.toString()
                    + " must implement ControlCameraDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_control_camera, null);

        builder.setView(view)
                .setTitle(R.string.dialog_title_control_camera)
                .setPositiveButton(R.string.close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        ImageButton upButton = (ImageButton) view.findViewById(R.id.up_button);
        upButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onUpClick(ControlCameraDialogFragment.this);
            }
        });

        ImageButton downButton = (ImageButton) view.findViewById(R.id.down_button);
        downButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onDownClick(ControlCameraDialogFragment.this);
            }
        });

        ImageButton rightButton = (ImageButton) view.findViewById(R.id.right_button);
        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onRightClick(ControlCameraDialogFragment.this);
            }
        });

        ImageButton leftButton = (ImageButton) view.findViewById(R.id.left_button);
        leftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onLeftClick(ControlCameraDialogFragment.this);
            }
        });

        ImageButton zoomInButton = (ImageButton) view.findViewById(R.id.zoom_in_button);
        zoomInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onZoomInClick(ControlCameraDialogFragment.this);
            }
        });

        ImageButton zoomOutButton = (ImageButton) view.findViewById(R.id.zoom_out_button);
        zoomOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onZoomOutClick(ControlCameraDialogFragment.this);
            }
        });

        return builder.create();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        final Activity activity = getActivity();
        if (activity instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) activity).onDismiss(dialog);
        }
    }

    public interface ControlCameraDialogListener {
        void onRightClick(ControlCameraDialogFragment dialogFragment);
        void onLeftClick(ControlCameraDialogFragment dialogFragment);
        void onDownClick(ControlCameraDialogFragment dialogFragment);
        void onUpClick(ControlCameraDialogFragment dialogFragment);
        void onZoomInClick(ControlCameraDialogFragment dialogFragment);
        void onZoomOutClick(ControlCameraDialogFragment dialogFragment);
    }
}
