package pl.edu.agh.placzkie.ctp_supervisor.model;

/**
 * Created by Leszek Placzkiewicz on 2016-09-03.
 */
public enum ZoomMode {
    ZOOM_IN, ZOOM_OUT;
}
