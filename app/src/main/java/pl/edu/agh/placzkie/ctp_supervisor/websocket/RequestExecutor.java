package pl.edu.agh.placzkie.ctp_supervisor.websocket;

import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;

import java.util.Collections;
import java.util.Map;

import pl.edu.agh.placzkie.ctp_supervisor.model.CameraDirection;
import pl.edu.agh.placzkie.ctp_supervisor.model.FocusType;
import pl.edu.agh.placzkie.ctp_supervisor.model.ZoomMode;
import pl.edu.agh.placzkie.ctp_supervisor.ws_service.WebSocketService;

import static pl.edu.agh.placzkie.ctp_supervisor.websocket.AdditionalMessagePropertiesConstants.KEY_AUTOMATIC_LECTURE_MODE;
import static pl.edu.agh.placzkie.ctp_supervisor.websocket.AdditionalMessagePropertiesConstants.KEY_CONFERENCE_NAME;
import static pl.edu.agh.placzkie.ctp_supervisor.websocket.AdditionalMessagePropertiesConstants.KEY_CP_LAYOUT;
import static pl.edu.agh.placzkie.ctp_supervisor.websocket.AdditionalMessagePropertiesConstants.KEY_FOCUS_TYPE;
import static pl.edu.agh.placzkie.ctp_supervisor.websocket.AdditionalMessagePropertiesConstants.KEY_MESSAGE_TEXT;
import static pl.edu.agh.placzkie.ctp_supervisor.websocket.AdditionalMessagePropertiesConstants.KEY_PARTICIPANT_NAME;
import static pl.edu.agh.placzkie.ctp_supervisor.websocket.AdditionalMessagePropertiesConstants.KEY_USERNAME;
import static pl.edu.agh.placzkie.ctp_supervisor.websocket.MessageType.END_CONFERENCE;
import static pl.edu.agh.placzkie.ctp_supervisor.websocket.MessageType.GET_CONFERENCES;
import static pl.edu.agh.placzkie.ctp_supervisor.websocket.MessageType.GET_CONFERENCE_INFO;
import static pl.edu.agh.placzkie.ctp_supervisor.websocket.MessageType.GET_PARTICIPANTS;
import static pl.edu.agh.placzkie.ctp_supervisor.websocket.MessageType.MOVE_CAMERA_DOWN;
import static pl.edu.agh.placzkie.ctp_supervisor.websocket.MessageType.MOVE_CAMERA_LEFT;
import static pl.edu.agh.placzkie.ctp_supervisor.websocket.MessageType.MOVE_CAMERA_RIGHT;
import static pl.edu.agh.placzkie.ctp_supervisor.websocket.MessageType.MOVE_CAMERA_UP;
import static pl.edu.agh.placzkie.ctp_supervisor.websocket.MessageType.SEND_MESSAGE;
import static pl.edu.agh.placzkie.ctp_supervisor.websocket.MessageType.SET_AUTOMATIC_LECTURE_MODE;
import static pl.edu.agh.placzkie.ctp_supervisor.websocket.MessageType.ZOOM_CAMERA_IN;
import static pl.edu.agh.placzkie.ctp_supervisor.websocket.MessageType.ZOOM_CAMERA_OUT;

/**
 * Created by Leszek Placzkiewicz on 2016-08-13.
 */
public class RequestExecutor {

    private static final String TAG = RequestExecutor.class.getName();


    private final String username;

    private final Messenger webSocketService;


    public RequestExecutor(String username, Messenger webSocketService) {
        this.username = Preconditions.checkNotNull(username);
        this.webSocketService = Preconditions.checkNotNull(webSocketService);
    }

    public void requestSetConferenceLayout(String conferenceName, String cpLayout) {
        MessageType messageType = MessageType.SET_CONFERENCE_LAYOUT;

        Map<String, String> additionalProperties = Maps.newHashMap();
        additionalProperties.put(KEY_CONFERENCE_NAME, conferenceName);
        additionalProperties.put(KEY_CP_LAYOUT, cpLayout);

        OutgoingMessage outgoingMessage = new OutgoingMessage(messageType, additionalProperties);

        sendToWebSocketService(outgoingMessage);
    }

    public void requestSetImportantParticipant(String participantName) {
        MessageType messageType = MessageType.SET_IMPORTANT_PARTICIPANT;

        Map<String, String> additionalProperties = Maps.newHashMap();
        additionalProperties.put(KEY_PARTICIPANT_NAME, participantName);

        OutgoingMessage outgoingMessage = new OutgoingMessage(messageType, additionalProperties);

        sendToWebSocketService(outgoingMessage);
    }

    public void requestDisconnectParticipant(String participantName) {
        MessageType messageType = MessageType.DISCONNECT_PARTICIPANT;

        Map<String, String> additionalProperties = Maps.newHashMap();
        additionalProperties.put(KEY_PARTICIPANT_NAME, participantName);

        OutgoingMessage outgoingMessage = new OutgoingMessage(messageType, additionalProperties);

        sendToWebSocketService(outgoingMessage);
    }

    public void requestSetFocusType(String conferenceName,
                                    FocusType focusType,
                                    String participantName) {
        MessageType messageType = MessageType.SET_FOCUS_TYPE;

        Map<String, String> additionalProperties = Maps.newHashMap();
        additionalProperties.put(KEY_CONFERENCE_NAME, conferenceName);
        additionalProperties.put(KEY_FOCUS_TYPE, convertToMcuFocusTypeName(focusType));
        if (focusType == FocusType.PARTICIPANT) {
            additionalProperties.put(KEY_PARTICIPANT_NAME, participantName);
        }

        OutgoingMessage outgoingMessage = new OutgoingMessage(messageType, additionalProperties);

        sendToWebSocketService(outgoingMessage);
    }

    private String convertToMcuFocusTypeName(FocusType focusType) {
        switch (focusType) {
            case VOICE_ACTIVATED:
                return "voiceActivated";
            case PARTICIPANT:
                return "participant";
//            case H239:
//                return "h239";
            default:
                throw new RuntimeException("Unknown focus type");
        }
    }

    public void requestSwitchMicrophone(String recipient, boolean isChecked) {
        MessageType messageType = isChecked ?
                MessageType.TURN_MICROPHONE_ON : MessageType.TURN_MICROPHONE_OFF;

        OutgoingMessage outgoingMessage =
                new OutgoingMessage(messageType, Collections.<String, String>emptyMap(), recipient);

        sendToWebSocketService(outgoingMessage);
    }

    public void requestZoomCamera(String recipient, ZoomMode zoomMode) {
        MessageType messageType = null;
        switch (zoomMode) {
            case ZOOM_IN:
                messageType = ZOOM_CAMERA_IN;
                break;
            case ZOOM_OUT:
                messageType = ZOOM_CAMERA_OUT;
                break;
        }

        OutgoingMessage outgoingMessage =
                new OutgoingMessage(messageType, Collections.<String, String>emptyMap(), recipient);
        sendToWebSocketService(outgoingMessage);
    }

    public void requestMoveCamera(String recipient, CameraDirection cameraDirection) {
        MessageType messageType = null;
        switch (cameraDirection) {
            case DOWN:
                messageType = MOVE_CAMERA_DOWN;
                break;
            case UP:
                messageType = MOVE_CAMERA_UP;
                break;
            case LEFT:
                messageType = MOVE_CAMERA_LEFT;
                break;
            case RIGHT:
                messageType = MOVE_CAMERA_RIGHT;
                break;
        }

        OutgoingMessage outgoingMessage =
                new OutgoingMessage(messageType, Collections.<String, String>emptyMap(), recipient);
        sendToWebSocketService(outgoingMessage);
    }

    public void requestSendMessage(String recipient, String message) {
        Map<String, String> additionalProperties = Maps.newHashMap();
        additionalProperties.put(KEY_MESSAGE_TEXT, message);

        OutgoingMessage outgoingMessage =
                new OutgoingMessage(SEND_MESSAGE, additionalProperties, recipient);
        sendToWebSocketService(outgoingMessage);
    }

    public void requestParticipantsList(String conferenceName) {
        Map<String, String> additionalProperties = Maps.newHashMap();
        additionalProperties.put(KEY_CONFERENCE_NAME, conferenceName);
        additionalProperties.put(KEY_USERNAME, username);

        OutgoingMessage outgoingMessage =
                new OutgoingMessage(GET_PARTICIPANTS, additionalProperties);
        sendToWebSocketService(outgoingMessage);
    }

    public void requestConferenceList() {
        Map<String, String> additionalProperties = Maps.newHashMap();
        additionalProperties.put(KEY_USERNAME, username);

        OutgoingMessage outgoingMessage =
                new OutgoingMessage(GET_CONFERENCES, additionalProperties);
        sendToWebSocketService(outgoingMessage);
    }

//    public void requestConferenceConfig(String conferenceName) {
//        Map<String, String> additionalProperties = Maps.newHashMap();
//        additionalProperties.put(KEY_USERNAME, username);
//        additionalProperties.put(KEY_CONFERENCE_NAME, conferenceName);
//
//        OutgoingMessage outgoingMessage =
//                new OutgoingMessage(GET_CONFERENCE_CONFIG, additionalProperties);
//        sendToWebSocketService(outgoingMessage);
//    }

    public void requestConferenceInfo(String conferenceName) {
        Map<String, String> additionalProperties = Maps.newHashMap();
        additionalProperties.put(KEY_USERNAME, username);
        additionalProperties.put(KEY_CONFERENCE_NAME, conferenceName);

        OutgoingMessage outgoingMessage =
                new OutgoingMessage(GET_CONFERENCE_INFO, additionalProperties);
        sendToWebSocketService(outgoingMessage);
    }

    public void requestEndConference(String conferenceName) {
        Map<String, String> additionalProperties = Maps.newHashMap();
        additionalProperties.put(KEY_USERNAME, username);
        additionalProperties.put(KEY_CONFERENCE_NAME, conferenceName);

        OutgoingMessage outgoingMessage = new OutgoingMessage(END_CONFERENCE, additionalProperties);

        sendToWebSocketService(outgoingMessage);
    }

    public void requestSetAutomaticLectureMode(String conferenceName, String automaticLectureMode) {
        Map<String, String> additionalProperties = Maps.newHashMap();
        additionalProperties.put(KEY_USERNAME, username);
        additionalProperties.put(KEY_CONFERENCE_NAME, conferenceName);
        additionalProperties.put(KEY_AUTOMATIC_LECTURE_MODE, automaticLectureMode);

        OutgoingMessage outgoingMessage = new OutgoingMessage(
                                                SET_AUTOMATIC_LECTURE_MODE, additionalProperties);

        sendToWebSocketService(outgoingMessage);
    }

    private void sendToWebSocketService(OutgoingMessage outgoingMessage) {
        try {
            webSocketService.send(
                    Message.obtain(null, WebSocketService.MSG_SEND_TO_SERVER, outgoingMessage));
        } catch (RemoteException e) {
            Log.e(TAG, e.getMessage());
        }
    }
}