package pl.edu.agh.placzkie.ctp_supervisor.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import pl.edu.agh.placzkie.ctp_supervisor.R;
import pl.edu.agh.placzkie.ctp_supervisor.activities.additions.IntentConstants;
import pl.edu.agh.placzkie.ctp_supervisor.ws_service.WebSocketService;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getName();


    private UserLoginTask mUserLoginTask = null;

    private AutoCompleteTextView mUsernameView;

    private EditText mPasswordView;

    private View mProgressView;

    private View mLoginFormView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setTitle(getResources().getText(R.string.activity_title_login));

        mUsernameView = (AutoCompleteTextView) findViewById(R.id.username);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mSignInButton = (Button) findViewById(R.id.sign_in_button);
        if (mSignInButton != null) {
            mSignInButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    attemptLogin();
                }
            });
        }

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        Toolbar mAppBar = (Toolbar) findViewById(R.id.loginAppBar);
        if (mAppBar != null) {
            mAppBar.setContentInsetsAbsolute(0, 0);
        }
        setSupportActionBar(mAppBar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.login_app_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startConfigurationActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void attemptLogin() {
        if (mUserLoginTask != null) {
            return;
        }

        if ( !isNetworkConnectionAvailable()) {
            Log.i(TAG, "No internet connection");
            Toast.makeText(getApplicationContext(),
                            R.string.error_no_internet_connection,
                            Toast.LENGTH_LONG).show();
            return;
        }

        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String username = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        } else if (!isUserNameValid(username)) {
            mUsernameView.setError(getString(R.string.error_invalid_username));
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mUserLoginTask = new UserLoginTask(username, password);
            mUserLoginTask.execute((Void) null);
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private void startConfigurationActivity() {
        Intent intent = new Intent(LoginActivity.this, ConfigurationActivity.class);
        startActivity(intent);
    }

    private boolean isNetworkConnectionAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    private boolean isUserNameValid(String username) {
        return true;
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }


    private final class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mUsername;

        private final String mPassword;

        private volatile Boolean connected;

        private volatile Boolean connDone;

        //web socket service stuff
        private Messenger mService = null;

        private boolean mIsBound;

        private final Messenger mMessenger = new Messenger(new IncomingHandler());

        private final ServiceConnection mConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName className, IBinder service) {
                mService = new Messenger(service);

                try {
                    Message msg = Message.obtain(null,
                                                WebSocketService.MSG_SIGN_IN,
                                                mUsername + ":" + mPassword);
                    msg.replyTo = mMessenger;
                    mService.send(msg);

                } catch (RemoteException e) {
                    /*Nothing we can do*/
                    Log.e(TAG, e.getMessage());
                }

                Log.i(TAG, "Connected to web socket service");
            }

            public void onServiceDisconnected(ComponentName className) {
                mService = null;
                Log.i(TAG, "Disconnected from web socket service");
            }
        };


        private UserLoginTask(String username, String password) {
            mUsername = username;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            startService(new Intent(LoginActivity.this, WebSocketService.class));
            doBindService();

            //TODO - consider using Locks instead
            int i = 0;
            while (i < 14) {
                if (connected != null) {
                    break;
                }

                try {
                    Thread.sleep(1000);
                    i++;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            doUnbindService();
            return connected;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mUserLoginTask = null;
            showProgress(false);

            if (success) {
                finish();
                startParticipantsActivity();
            } else {
                if(connDone) {
                    mPasswordView.setError(getString(R.string.error_incorrect_password));
                    mPasswordView.requestFocus();
                } else {
                    Toast toast = Toast.makeText(
                            getApplicationContext(), getString(R.string.error_server_unreachable),
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        }

        @Override
        protected void onCancelled() {
            mUserLoginTask = null;
            showProgress(false);
        }

        private void doBindService() {
            bindService(new Intent(LoginActivity.this, WebSocketService.class),
                        mConnection,
                        Context.BIND_AUTO_CREATE);
            mIsBound = true;
        }


        private void doUnbindService() {
            if (mIsBound) {
                unbindService(mConnection);
                mIsBound = false;
                Log.i(TAG, "Unbinding from service");
            }
        }

        private void startParticipantsActivity() {
            Intent intent = new Intent(LoginActivity.this, ConferencesActivity.class);
            intent.putExtra(IntentConstants.USERNAME, mUsername);
            startActivity(intent);
        }

        private class IncomingHandler extends Handler {

            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case WebSocketService.MSG_SIGN_IN:
                        connected = (Boolean) msg.obj;
                        break;
                    case WebSocketService.MSG_CONN_DONE:
                        connDone = (Boolean) msg.obj;
                        break;
                    default:
                        super.handleMessage(msg);
                }
            }
        }
    }

    //TODO - add unbinding to onDestroy method
}

