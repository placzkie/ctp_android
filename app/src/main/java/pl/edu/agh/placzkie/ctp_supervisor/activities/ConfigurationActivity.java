
package pl.edu.agh.placzkie.ctp_supervisor.activities;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.common.base.Strings;
import com.google.common.net.InetAddresses;

import java.util.List;

import pl.edu.agh.placzkie.ctp_supervisor.R;

import static android.preference.Preference.OnPreferenceChangeListener;

public class ConfigurationActivity extends AppCompatPreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (!super.onMenuItemSelected(featureId, item)) {
                NavUtils.navigateUpFromSameTask(this);
            }
            return true;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.pref_headers, target);

        setContentView(R.layout.activity_configuration);
        Toolbar toolbar = (Toolbar) findViewById(R.id.configurationAppBar);
        setSupportActionBar(toolbar);

        ActionBar bar = getSupportActionBar();
        bar.setHomeButtonEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setDisplayShowTitleEnabled(true);
        bar.setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        bar.setTitle(R.string.activity_title_configuration);
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || ConnectionPreferenceFragment.class.getName().equals(fragmentName);
    }

    private static boolean isPortNumberValid(String portNumber) {
        if ( Strings.isNullOrEmpty(portNumber)) {
            return false;
        } else if ( !TextUtils.isDigitsOnly(portNumber)) {
            return false;
        } else {
            int port = Integer.parseInt(portNumber);
            return port >= 1024 && port <= 65535;
        }
    }

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    private static void bindPreferenceSummaryToValue(Preference preference,
                                                     OnPreferenceChangeListener changeListener) {
        preference.setOnPreferenceChangeListener(changeListener);

        changeListener.onPreferenceChange(preference,
                PreferenceManager.getDefaultSharedPreferences(preference.getContext())
                                 .getString(preference.getKey(), ""));
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class ConnectionPreferenceFragment extends PreferenceFragment {

        private static EditTextPreference ipAddressPreference;

        private static EditTextPreference portPreference;

        private static OnPreferenceChangeListener ipAddressPreferenceChangeListener =
                new OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object o) {
                        String ipAddress = o.toString();

                        if (InetAddresses.isInetAddress(ipAddress)) {
                            preference.setSummary(ipAddress);
                            return true;
                        } else {
                            Toast.makeText(ipAddressPreference.getContext(),
                                            R.string.error_invalid_ip_address,
                                            Toast.LENGTH_LONG).show();
                            return false;
                        }
                    }
                };

        private static OnPreferenceChangeListener portPreferenceChangeListener =
                new OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object o) {
                        String portNumber = o.toString();

                        if (isPortNumberValid(portNumber)) {
                            preference.setSummary(portNumber);
                            return true;
                        } else {
                            Toast.makeText(portPreference.getContext(),
                                            R.string.error_invalid_port_number,
                                            Toast.LENGTH_LONG).show();
                            return false;
                        }
                    }
                };


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_connection);
            setHasOptionsMenu(true);

            ipAddressPreference = (EditTextPreference) findPreference(
                    obtainStringPreferenceKey(R.string.pref_key_ip_address));
            portPreference = (EditTextPreference) findPreference(
                    obtainStringPreferenceKey(R.string.pref_key_port));

            bindPreferenceSummaryToValue(ipAddressPreference, ipAddressPreferenceChangeListener);
            bindPreferenceSummaryToValue(portPreference, portPreferenceChangeListener);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), ConfigurationActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View layout = inflater.inflate(R.layout.activity_configuration, container, false);
            if (layout != null) {
                AppCompatPreferenceActivity activity = (AppCompatPreferenceActivity) getActivity();

                Toolbar toolbar = (Toolbar) layout.findViewById(R.id.configurationAppBar);
                activity.setSupportActionBar(toolbar);

                ActionBar bar = activity.getSupportActionBar();
                bar.setHomeButtonEnabled(true);
                bar.setDisplayHomeAsUpEnabled(true);
                bar.setDisplayShowTitleEnabled(true);
                bar.setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
                bar.setTitle(getPreferenceScreen().getTitle());
            }
            return layout;
        }

        @Override
        public void onResume() {
            super.onResume();

            if (getView() != null) {
                View frame = (View) getView().getParent();
                if (frame != null)
                    frame.setPadding(0, 0, 0, 0);
            }
        }

        private String obtainStringPreferenceKey(int resourceId) {
            return getResources().getText(resourceId).toString();
        }
    }
}
