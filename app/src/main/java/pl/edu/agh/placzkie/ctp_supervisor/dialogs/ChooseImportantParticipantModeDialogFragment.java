package pl.edu.agh.placzkie.ctp_supervisor.dialogs;


import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import org.greenrobot.eventbus.EventBus;

import pl.edu.agh.placzkie.ctp_supervisor.R;
import pl.edu.agh.placzkie.ctp_supervisor.model.ConferenceEvent;
import pl.edu.agh.placzkie.ctp_supervisor.model.FocusType;

/**
 * Created by Leszek Placzkiewicz on 2016-09-07.
 */
//TODO - rename
public class ChooseImportantParticipantModeDialogFragment extends DialogFragment {

    private int selectedMode;

    private ChooseImportantParticipantModeDialogListener mListener;

    private FocusType focusType;


    public int getSelectedMode() {
        return selectedMode;
    }

    public FocusType getSelectedFocusType() {
        return findItemByIndex(selectedMode);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (ChooseImportantParticipantModeDialogListener) activity;
        } catch (ClassCastException ex) {
            throw new ClassCastException(activity.toString()
                    + " must implement ChooseImportantParticipantModeDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setCurrentFocusType();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.dialog_title_choose_focus_type)
                .setSingleChoiceItems(R.array.focus_types, findItemIndex(focusType),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                selectedMode = which;
                            }
                        })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onClick(ChooseImportantParticipantModeDialogFragment.this);
                    }
                });

        return builder.create();
    }

    private void setCurrentFocusType() {
        ConferenceEvent conferenceEvent =
                EventBus.getDefault().getStickyEvent(ConferenceEvent.class);
        focusType = conferenceEvent.getConference().getFocusType();
    }

    private FocusType findItemByIndex(int index) {
        switch (index) {
            case 0:
                return FocusType.VOICE_ACTIVATED;
            case 1:
                return FocusType.PARTICIPANT;
//            case 2:
//                return FocusType.H239;
            default:
                throw new RuntimeException("Invalid index");
        }
    }

    private int findItemIndex(FocusType focusType) {
        if (focusType == null) {
            return 0;
        }

        switch (focusType) {
            case VOICE_ACTIVATED:
                return 0;
            case PARTICIPANT:
                return 1;
//            case H239:
//                return 2;
            default:
                throw new RuntimeException("Item index not found.");
        }
    }

    public interface ChooseImportantParticipantModeDialogListener {
        void onClick(ChooseImportantParticipantModeDialogFragment dialog);
    }
}
