package pl.edu.agh.placzkie.ctp_supervisor.websocket;

import com.google.common.base.Preconditions;
import com.google.gson.annotations.SerializedName;

import java.util.Collections;
import java.util.Map;

/**
 * Created by Leszek Placzkiewicz on 2016-08-13.
 */
public class OutgoingMessage {

    @SerializedName("messageType")
    private final MessageType messageType;

    @SerializedName("additionalProperties")
    private final Map<String, String> additionalProperties;

    @SerializedName("recipient")
    private final String recipient;


    public OutgoingMessage(MessageType messageType,
                           Map<String, String> additionalProperties,
                           String recipient) {
        this.messageType = Preconditions.checkNotNull(messageType);
        this.additionalProperties = Preconditions.checkNotNull(additionalProperties);
        this.recipient = recipient;
    }

    public OutgoingMessage(MessageType messageType, Map<String, String> additionalProperties) {
        this(messageType, additionalProperties, "");
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public Map<String, String> getAdditionalProperties() {
        return Collections.unmodifiableMap(additionalProperties);
    }

    public String getProperty(String name) {
        return additionalProperties.get(name);
    }

    public String getRecipient() {
        return recipient;
    }

    @Override
    public String toString() {
        return "OutgoingMessage{" +
                "messageType=" + messageType +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}
