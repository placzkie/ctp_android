package pl.edu.agh.placzkie.ctp_supervisor.websocket;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import pl.edu.agh.placzkie.ctp_supervisor.model.ConferenceInfo;
import pl.edu.agh.placzkie.ctp_supervisor.model.FocusType;
import pl.edu.agh.placzkie.ctp_supervisor.model.Participant;

/**
 * Created by Leszek Placzkiewicz on 2016-08-13.
 */
public class MessageParser {

    private final Gson gson;


    public MessageParser() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(ServerMessage.class, new ServerMessageTypeAdapter());
        gson = gsonBuilder.create();
    }

    public String toJson(OutgoingMessage outgoingMessage) {
        return gson.toJson(outgoingMessage);
    }

    public ServerMessage fromJson(String message) {
        return gson.fromJson(message, ServerMessage.class);
    }

    private final class ServerMessageTypeAdapter extends TypeAdapter<ServerMessage> {
        @Override
        public void write(JsonWriter out, ServerMessage value) throws IOException {
            throw new UnsupportedOperationException("Serializing ServerMessage type");
        }

        @Override
        public ServerMessage read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }

            MessageType messageType = null;
            Map<String, Object> additionalProperties = Maps.newHashMap();
            in.beginObject();
            while (in.hasNext()) {
                switch (in.nextName()) {
                    case ServerMessageConstants.KEY_MESSAGE_TYPE:
                        messageType = MessageType.valueOf(in.nextString());
                        break;
                    case ServerMessageConstants.KEY_USERNAME:
                        in.nextString();
                        break;
                    case ServerMessageConstants.KEY_CONFERENCE_NAME:
                        in.nextString();
                        break;
                    case ServerMessageConstants.KEY_ADDITIONAL_PROPERTIES:
                        in.beginObject();
                        while (in.hasNext()) {
                            switch (in.nextName()) {
                                case ServerMessageConstants.KEY_AP_PARTICIPANTS:
                                    readAndAddParticipantList(in,
                                            ServerMessageConstants.KEY_AP_PARTICIPANTS,
                                            additionalProperties);
                                    break;
                                case ServerMessageConstants.KEY_AP_CONFERENCES:
                                    readAndAddList(in,
                                            ServerMessageConstants.KEY_AP_CONFERENCES,
                                            additionalProperties);
                                    break;
                                case ServerMessageConstants.KEY_AP_CONFERENCE:
                                    readAndAddConference(in,
                                            ServerMessageConstants.KEY_AP_CONFERENCE,
                                            additionalProperties);
                            }
                        }
                        in.endObject();
                        break;
                }
            }
            in.endObject();
            return new ServerMessage(messageType, additionalProperties);
        }

        private void readAndAddConference(JsonReader in,
                                          String propertyName,
                                          Map<String, Object> additionalProperties)
                                                                            throws IOException {
            String conferenceName = null;
//            FocusType focusType = null;
//            String focusParticipant = null;
//            int currentLayout = 0;
//            String layoutSource = null;
            String activeStartTime = null;
            String activeEndTime = null;

            in.beginObject();
            while (in.hasNext()) {
                switch (in.nextName()) {
                    case ServerMessageConstants.KEY_AP_CONFERENCE_CONFERENCE_NAME:
                        conferenceName = in.nextString();
                        break;
                    case ServerMessageConstants.KEY_AP_CONFERENCE_ACTIVE_START_TIME:
                        activeStartTime = in.nextString();
                        break;
                    case ServerMessageConstants.KEY_AP_CONFERENCE_ACTIVE_END_TIME:
                        activeEndTime = in.nextString();
                        break;

//                    case ServerMessageConstants.KEY_AP_CONFERENCE_FOCUS_TYPE:
//                        focusType = parseFocusType(in.nextString());
//                        break;
//                    case ServerMessageConstants.KEY_AP_CONFERENCE_FOCUS_PARTICIPANT_NAME:
//                        focusParticipant = in.nextString();
//                        break;
//                    case ServerMessageConstants.KEY_AP_CONFERENCE_CURRENT_LAYOUT:
//                        currentLayout = in.nextInt();
//                        break;
//                    case ServerMessageConstants.KEY_AP_CONFERENCE_LAYOUT_SOURCE:
//                        layoutSource = in.nextString();
//                        break;
                }
            }
            in.endObject();
            additionalProperties.put(propertyName,
                        new ConferenceInfo(conferenceName, activeStartTime, activeEndTime));
//                        new Conference(focusType, focusParticipant, currentLayout, layoutSource));
        }

        private FocusType parseFocusType(String focusTypeString) {
            switch (focusTypeString) {
                case "voiceActivated":
                    return FocusType.VOICE_ACTIVATED;
                case "participant":
                    return FocusType.PARTICIPANT;
//                case "h239":
//                    return FocusType.H239;
                default:
                    throw new UnsupportedOperationException("Unknown focus type");
            }
        }

        private void readAndAddList(JsonReader input,
                                    String propertyName,
                                    Map<String, Object> additionalProperties) throws IOException {
            List<String> list = Lists.newArrayList();

            input.beginArray();
            while (input.hasNext()) {
                list.add(input.nextString());
            }
            input.endArray();
            additionalProperties.put(propertyName, list);
        }

        private void readAndAddParticipantList(
                                    JsonReader input,
                                    String propertyName,
                                    Map<String, Object> additionalProperties) throws IOException {

            List<Participant> participants = Lists.newArrayList();

            String participantName = "";
            String displayName = "";
            boolean audioMuted = false;
            boolean activeSpeaker = false;
            boolean lecturer = false;
            boolean important = false;
            String participantType = "";
            boolean lecturerGroup = false;

            input.beginArray();
            while (input.hasNext()) {
                input.beginObject();
                while (input.hasNext()) {
                    switch (input.nextName()) {
                        case ServerMessageConstants.KEY_AP_PARTICIPANTS_NAME:
                            participantName = input.nextString();
                            break;
                        case ServerMessageConstants.KEY_AP_PARTICIPANTS_AUDIO_MUTED:
                            audioMuted = input.nextBoolean();
                            break;
                        case ServerMessageConstants.KEY_AP_PARTICIPANTS_ACTIVE_SPEAKER:
                            activeSpeaker = input.nextBoolean();
                            break;
                        case ServerMessageConstants.KEY_AP_PARTICIPANTS_DISPLAY_NAME:
                            displayName = input.nextString();
                            break;
                        case ServerMessageConstants.KEY_AP_PARTICIPANTS_IMPORTANT:
                            important = input.nextBoolean();
                            break;
                        case ServerMessageConstants.KEY_AP_PARTICIPANTS_LECTURER:
                            lecturer = input.nextBoolean();
                            break;
                        case ServerMessageConstants.KEY_AP_PARTICIPANTS_PARTICIPANT_TYPE:
                            participantType = input.nextString();
                            break;
                        case ServerMessageConstants.KEY_AP_PARTICIPANTS_LECTURER_GROUP:
                            lecturerGroup = input.nextBoolean();
                            break;
                    }
                }
                input.endObject();
                participants.add(new Participant(participantName, audioMuted, displayName,
                        important, activeSpeaker, lecturer, participantType, lecturerGroup));
            }
            input.endArray();

            additionalProperties.put(propertyName, participants);
        }
    }
}
