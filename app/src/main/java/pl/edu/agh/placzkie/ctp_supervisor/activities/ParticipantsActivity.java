package pl.edu.agh.placzkie.ctp_supervisor.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.Nullable;

import pl.edu.agh.placzkie.ctp_supervisor.R;
import pl.edu.agh.placzkie.ctp_supervisor.activities.adapter.ParticipantsAdapter;
import pl.edu.agh.placzkie.ctp_supervisor.activities.additions.DividerItemDecoration;
import pl.edu.agh.placzkie.ctp_supervisor.activities.additions.IntentConstants;
import pl.edu.agh.placzkie.ctp_supervisor.dialogs.ChooseConferenceLayoutDialogFragment;
import pl.edu.agh.placzkie.ctp_supervisor.dialogs.ChooseImportantParticipantModeDialogFragment;
import pl.edu.agh.placzkie.ctp_supervisor.dialogs.ControlCameraDialogFragment;
import pl.edu.agh.placzkie.ctp_supervisor.dialogs.SendMessageDialogFragment;
import pl.edu.agh.placzkie.ctp_supervisor.model.CameraDirection;
import pl.edu.agh.placzkie.ctp_supervisor.model.Conference;
import pl.edu.agh.placzkie.ctp_supervisor.model.ConferenceEvent;
import pl.edu.agh.placzkie.ctp_supervisor.model.ConferenceInfo;
import pl.edu.agh.placzkie.ctp_supervisor.model.FocusType;
import pl.edu.agh.placzkie.ctp_supervisor.model.Participant;
import pl.edu.agh.placzkie.ctp_supervisor.model.ZoomMode;
import pl.edu.agh.placzkie.ctp_supervisor.websocket.MessageType;
import pl.edu.agh.placzkie.ctp_supervisor.websocket.RequestExecutor;
import pl.edu.agh.placzkie.ctp_supervisor.websocket.ServerMessage;
import pl.edu.agh.placzkie.ctp_supervisor.websocket.ServerMessageConstants;
import pl.edu.agh.placzkie.ctp_supervisor.ws_service.WebSocketService;

public class ParticipantsActivity extends AppCompatActivity implements
        SendMessageDialogFragment.SendMessageDialogListener,
        ControlCameraDialogFragment.ControlCameraDialogListener,
        ChooseConferenceLayoutDialogFragment.ChooseConferenceLayoutDialogListener,
        ChooseImportantParticipantModeDialogFragment.ChooseImportantParticipantModeDialogListener,
        ParticipantsAdapter.ParticipantViewHolder.ClickListener,
        ParticipantsAdapter.SectionViewHolder.ClickListener,
        DialogInterface.OnDismissListener {

    private static final String TAG = ParticipantsActivity.class.getName();

    private static final String CHOOSE_CONFERENCE_LAYOUT_DIALOG_FRAGMENT_TAG =
                                                            "ChooseConferenceLayoutDialogFragment";

    private static final String CHOOSE_IMPORTANT_PARTICIPANT_MODE_DIALOG_FRAGMENT_TAG =
                                                    "ChooseImportantParticipantModeDialogFragment";

    private static final String SEND_MESSAGE_DIALOG_FRAGMENT_TAG = "SendMessageDialogFragment";

    private static final String CONTROL_CAMERA_DIALOG_FRAGMENT_TAG = "ControlCameraDialogFragment";

    private TextView almTextView;

    private TextView layoutNameTextView;

    private TextView focusTypeTextView;

    private TextView startTimeTextView;

    private TextView endTimeTextView;

    private Button endConferenceButton;

    private RecyclerView recyclerView;

    private List<Participant> participantList = new ArrayList<>();

    private ParticipantsAdapter participantsAdapter;

    private RequestExecutor requestExecutor;

    private String username;

    private String conference; // TODO - conference name will be obtained from preceding activity

    private Conference conferenceConfig;

    private ConferenceInfo conf;

    private FocusType selectedFocusType;

    private ActionMode mActionMode;

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.participants_contextual_menu, menu);

            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            int count = participantsAdapter.getSelectedItemCount();
            MenuItem moveCameraItem = menu.findItem(R.id.move_camera);
            MenuItem disconnectItem = menu.findItem(R.id.disconnect);
//            MenuItem markAsImportantItem = menu.findItem(R.id.mark_as_important);
            if(count > 1) {
                moveCameraItem.setVisible(false);
                disconnectItem.setVisible(false);
//                markAsImportantItem.setVisible(false);
                return true;
            } else if (count == 1) {
                moveCameraItem.setVisible(true);
                disconnectItem.setVisible(true);
//                markAsImportantItem.setVisible(true);
                return true;
            }
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.move_camera:
                    showControlCameraDialog();
                    return true;
                case R.id.send_message:
                    showSendMessageDialog();
                    return true;
                case R.id.disconnect:
                    handleDisconnect();
                    return true;
//                case R.id.mark_as_important:
//                    handleMarkAsImportant();
//                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            participantsAdapter.clearSelection();
            mActionMode = null;
        }
    };

    //WebSocketService variables
    private volatile boolean mIsBound;

    private Messenger mService = null;

    private final Messenger mMessenger = new Messenger(new IncomingHandler());

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = new Messenger(service);

            try {
                Message msg = Message.obtain(null, WebSocketService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);

            } catch (RemoteException e) {
                    /*Nothing we can do*/
                Log.e(TAG, e.getMessage());
            }

            Log.i(TAG, "Web socket service connected.");
            mIsBound = true;
        }

        public void onServiceDisconnected(ComponentName className) {
            mService = null;
            Log.i(TAG, "Web socket service disconnected.");
        }
    };

    private void sendInitialConferenceConfigToServer() {
        boolean automaticLectureModeEnabled = conferenceConfig.isAutomaticLectureModeEnabled();
        String automaticLectureMode;
        if (automaticLectureModeEnabled) {
            // type2 - enable ALM immediately
            automaticLectureMode = "type2";
        } else {
            automaticLectureMode = "disabled";
        }
        requestExecutor.requestSetAutomaticLectureMode(conference, automaticLectureMode);

        FocusType focusType = conferenceConfig.getFocusType();
        requestExecutor.requestSetFocusType(conference, focusType, "");

        String cpLayout = conferenceConfig.getCpLayout();
        requestExecutor.requestSetConferenceLayout(conference, cpLayout);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_participants);

        Intent intent = getIntent();
        username = intent.getStringExtra(IntentConstants.USERNAME);
        conference = intent.getStringExtra(IntentConstants.CONFERENCE);
        Log.i(TAG, "Username = " + username);

        conferenceConfig = Conference.createDefault();
        EventBus.getDefault().postSticky(new ConferenceEvent(conferenceConfig));

        Toolbar myToolbar = (Toolbar) findViewById(R.id.participantsAppBar);
        setSupportActionBar(myToolbar);

        almTextView = (TextView) findViewById(R.id.alm_text_view);
        almTextView.setText(getString(R.string.alm_state,
                conferenceConfig.isAutomaticLectureModeEnabled() ? "enabled" : "disabled"));

        layoutNameTextView = (TextView) findViewById(R.id.layout_name_text_view);
        layoutNameTextView.setText(getString(R.string.layout_name,
                getResources().getStringArray(R.array.conference_layout_modes)[0]));
        layoutNameTextView.setVisibility(conferenceConfig.isAutomaticLectureModeEnabled() ?
                                                                    View.INVISIBLE : View.VISIBLE);

        focusTypeTextView = (TextView) findViewById(R.id.focus_type_text_view);
        focusTypeTextView.setText(getString(R.string.focus_type_name,
                                                conferenceConfig.getFocusType().getDescription()));
        focusTypeTextView.setVisibility(conferenceConfig.isAutomaticLectureModeEnabled() ?
                                                                    View.INVISIBLE : View.VISIBLE);

        startTimeTextView = (TextView) findViewById(R.id.start_time_text_view);
        endTimeTextView = (TextView) findViewById(R.id.end_time_text_view);

        endConferenceButton = (Button) findViewById(R.id.end_conference_button);
        endConferenceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestExecutor.requestEndConference(conference);
                finish();
            }
        });

        participantsAdapter = new ParticipantsAdapter(participantList, this, this);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager =
                new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(
                new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(participantsAdapter);

        //TODO - consider using execute().get(timeout), instead of postExecute()
        new MessengerServiceConnectTask().execute((Void) null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.participants_app_bar, menu);
//        findViewById(R.id.action_set_layout).setVisibility(View.INVISIBLE);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean almEnabled = conferenceConfig.isAutomaticLectureModeEnabled();
        menu.findItem(R.id.checkbox_automatic_lecture_mode).setChecked(almEnabled);
        if(almEnabled) {
            menu.findItem(R.id.action_set_layout).setVisible(false);
            menu.findItem(R.id.action_focus_type).setVisible(false);
            return true;
        } else {
            menu.findItem(R.id.action_set_layout).setVisible(true);
            menu.findItem(R.id.action_focus_type).setVisible(true);
            return true;
        }
    }

    private void updateConferenceConfig(boolean isChecked) {
        conferenceConfig.setAutomaticLectureModeEnabled(!isChecked);
        almTextView.setText(getString(R.string.alm_state,
                conferenceConfig.isAutomaticLectureModeEnabled() ? "enabled" : "disabled"));
        layoutNameTextView.setVisibility(conferenceConfig.isAutomaticLectureModeEnabled() ?
                                                                    View.INVISIBLE : View.VISIBLE);
        focusTypeTextView.setVisibility(conferenceConfig.isAutomaticLectureModeEnabled() ?
                                                                    View.INVISIBLE : View.VISIBLE);

        requestExecutor.requestSetAutomaticLectureMode(conference, !isChecked ? "type2" : "disabled");
        participantsAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.checkbox_automatic_lecture_mode:
                updateConferenceConfig(item.isChecked());
                return true;
            case R.id.action_set_layout:
                showChooseConferenceLayoutDialog();
                return true;
            case R.id.action_focus_type:
                showSetFocusTypeDialog();
                return true;
            case R.id.action_sign_out:
                signOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        doUnbindService();
        super.onDestroy();
    }

    @Override
    public void onItemClicked(int position) {
        if (mActionMode != null) {
            toggleSelection(position);
        } else if (selectedFocusType == FocusType.PARTICIPANT && !conferenceConfig.isAutomaticLectureModeEnabled()){
//            String participantName = participantList.get(position).getName();
            Participant participant = getParticipantForIndex(position);
            String participantName = getParticipantForIndex(position).getName();
            String participantDisplayName = getParticipantForIndex(position).getDisplayName();

            Toast.makeText(getApplicationContext(),
                    (participant.getParticipantType().equals("ad_hoc") ?
                            participantDisplayName : participantName) + " will dominate layout",
                            Toast.LENGTH_LONG).show();

//            conferenceConfig.setFocusParticipant(participantName);
            conferenceConfig.setFocusParticipant(participant.getParticipantType().equals("ad_hoc") ?
                    participantDisplayName : participantName);
            requestExecutor.requestSetFocusType(conference, selectedFocusType, participantName);
            participantsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onItemLongClicked(int position) {
        if (mActionMode == null) {
            mActionMode = startSupportActionMode(mActionModeCallback);
        }

        toggleSelection(position);
        return true;
    }

    @Override
    public void onDialogPositiveClick(SendMessageDialogFragment dialog) {
        Log.i(TAG, "onDialogPositiveClick");
        List<String> multipleSelectedParticipantNames = obtainMultipleSelectedParticipantNames();

        for (String participantName : multipleSelectedParticipantNames) {
            requestExecutor.requestSendMessage(participantName, dialog.getMessage());
        }
    }

    @Override
    public void onDialogNegativeClick(SendMessageDialogFragment dialog) {
        Log.i(TAG, "onDialogNegativeClick");
    }

    @Override
    public void onRightClick(ControlCameraDialogFragment dialogFragment) {
        String singleSelectedParticipantName = obtainSingleSelectedParticipantName();
        Log.i(TAG, "onRightClick" + " " + singleSelectedParticipantName);
        requestExecutor.requestMoveCamera(singleSelectedParticipantName, CameraDirection.RIGHT);
    }

    @Override
    public void onLeftClick(ControlCameraDialogFragment dialogFragment) {
        String singleSelectedParticipantName = obtainSingleSelectedParticipantName();
        Log.i(TAG, "onLeftClick" + " " + singleSelectedParticipantName);
        requestExecutor.requestMoveCamera(singleSelectedParticipantName, CameraDirection.LEFT);
    }

    @Override
    public void onDownClick(ControlCameraDialogFragment dialogFragment) {
        String singleSelectedParticipantName = obtainSingleSelectedParticipantName();
        Log.i(TAG, "onDownClick" + " " + singleSelectedParticipantName);
        requestExecutor.requestMoveCamera(singleSelectedParticipantName, CameraDirection.DOWN);
    }

    @Override
    public void onUpClick(ControlCameraDialogFragment dialogFragment) {
        String singleSelectedParticipantName = obtainSingleSelectedParticipantName();
        Log.i(TAG, "onUpClick" + " " + singleSelectedParticipantName);
        requestExecutor.requestMoveCamera(singleSelectedParticipantName, CameraDirection.UP);
    }

    @Override
    public void onZoomInClick(ControlCameraDialogFragment dialogFragment) {
        String singleSelectedParticipantName = obtainSingleSelectedParticipantName();
        Log.i(TAG, "onZoomInClick" + " " + singleSelectedParticipantName);
        requestExecutor.requestZoomCamera(singleSelectedParticipantName, ZoomMode.ZOOM_IN);
    }

    @Override
    public void onZoomOutClick(ControlCameraDialogFragment dialogFragment) {
        String singleSelectedParticipantName = obtainSingleSelectedParticipantName();
        Log.i(TAG, "onZoomOutClick" + " " + singleSelectedParticipantName);
        requestExecutor.requestZoomCamera(singleSelectedParticipantName, ZoomMode.ZOOM_OUT);
    }

    @Override
    public void onClick(ChooseConferenceLayoutDialogFragment dialog) {
        String cpLayout = dialog.getCpLayout();
        int selectedMode = dialog.getSelectedMode();
        Log.i(TAG, "onClick " + dialog.getClass().getName() + "cpLayout : " + cpLayout);

        conferenceConfig.setCpLayout(cpLayout);
        layoutNameTextView.setText(getString(R.string.layout_name,
                    getResources().getStringArray(R.array.conference_layout_modes)[selectedMode]));
        requestExecutor.requestSetConferenceLayout(conference, cpLayout);
    }

    @Override
    public void onClick(ChooseImportantParticipantModeDialogFragment dialog) {
        selectedFocusType = dialog.getSelectedFocusType();
        Log.i(TAG, "onClick " + dialog.getClass().getName() +
                                                "selected focus type : " + selectedFocusType);

        conferenceConfig.setFocusType(selectedFocusType);
        focusTypeTextView.setText(getString(R.string.focus_type_name,
                                                conferenceConfig.getFocusType().getDescription()));

        if (selectedFocusType != FocusType.PARTICIPANT) {
            requestExecutor.requestSetFocusType(conference, selectedFocusType, "");
        }
    }

    @Override
    public void onSwitchClicked(int position, boolean isChecked) {
        Log.i(TAG, "onSwitchClicked" + position + " " + isChecked);
//        Participant participant = participantList.get(position);
        Participant participant = getParticipantForIndex(position);
        participant.setAudioMuted(!isChecked);
        requestExecutor.requestSwitchMicrophone(participant.getName(), isChecked);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        Log.i(TAG, "onDismiss");
        if (mActionMode != null) {
            mActionMode.finish();
        }
    }

    @Override
    public void enableAllMicrophonesClicked() {
        Log.i(TAG, "enableAllMicrophonesClicked");
        for (Participant participant : participantList.subList(1, participantList.size())) {
            participant.setAudioMuted(false);
            requestExecutor.requestSwitchMicrophone(participant.getName(), true);
        }
    }

    @Override
    public void disableAllMicrophonesClicked() {
        Log.i(TAG, "disableAllMicrophonesClicked");
        for (Participant participant : participantList.subList(1, participantList.size())) {
            participant.setAudioMuted(true);
            requestExecutor.requestSwitchMicrophone(participant.getName(), false);
        }
    }

    private void handleMarkAsImportant() {
        String singleSelectedParticipantName = obtainSingleSelectedParticipantName();
        Log.i(TAG, "markAsImportant : " + singleSelectedParticipantName);

        requestExecutor.requestSetImportantParticipant(singleSelectedParticipantName);

        if (mActionMode != null) {
            mActionMode.finish();
        }
    }

    private void handleDisconnect() {
        String singleSelectedParticipantName = obtainSingleSelectedParticipantName();
        Log.i(TAG, "Disconnect participant: " + singleSelectedParticipantName);

        requestExecutor.requestDisconnectParticipant(singleSelectedParticipantName);

        if (mActionMode != null) {
            mActionMode.finish();
        }
    }

    private void showSendMessageDialog() {
        SendMessageDialogFragment sendMessageDialogFragment = new SendMessageDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("participantsNames",
                                        new ArrayList<>(obtainMultipleSelectedParticipantVisibleNames()));
        sendMessageDialogFragment.setArguments(bundle);
        sendMessageDialogFragment.show(getSupportFragmentManager(),
                SEND_MESSAGE_DIALOG_FRAGMENT_TAG);
    }

    private void showControlCameraDialog() {
        ControlCameraDialogFragment controlCameraDialogFragment = new ControlCameraDialogFragment();
        controlCameraDialogFragment.show(getSupportFragmentManager(),
                CONTROL_CAMERA_DIALOG_FRAGMENT_TAG);
    }

    private void showChooseConferenceLayoutDialog() {
        ChooseConferenceLayoutDialogFragment chooseConferenceLayoutDialogFragment =
                new ChooseConferenceLayoutDialogFragment();
        chooseConferenceLayoutDialogFragment.show(getSupportFragmentManager(),
                CHOOSE_CONFERENCE_LAYOUT_DIALOG_FRAGMENT_TAG);
    }

    private void showSetFocusTypeDialog() {
        ChooseImportantParticipantModeDialogFragment chooseImportantParticipantModeDialogFragment =
                new ChooseImportantParticipantModeDialogFragment();
        chooseImportantParticipantModeDialogFragment.show(getSupportFragmentManager(),
                CHOOSE_IMPORTANT_PARTICIPANT_MODE_DIALOG_FRAGMENT_TAG);
    }

    private String obtainSingleSelectedParticipantName() {
        List<String> participantNames = obtainMultipleSelectedParticipantNames();
        return participantNames.get(0);
    }

    private List<String> obtainMultipleSelectedParticipantNames() {
        List<Integer> selectedItems = participantsAdapter.getSelectedItems();
        return Lists.transform(selectedItems, indexToParticipantNameFunction());
    }

    private List<String> obtainMultipleSelectedParticipantVisibleNames() {
        List<Integer> selectedItems = participantsAdapter.getSelectedItems();
        return Lists.transform(selectedItems, indexToParticipantVisibleNameFunction());
    }

    private Function<Integer, String> indexToParticipantNameFunction() {
        return new Function<Integer, String>() {
            @Override
            public String apply(@Nullable Integer input) {
                if (input == 1) {
                    return participantList.get(0).getName();
                } else if (input > 2) {
                    return participantList.get(input - 2).getName();
                }
//                return participantList.get(input).getName();
                throw new RuntimeException("Invalid index");
            }
        };
    }

    private Function<Integer, String> indexToParticipantVisibleNameFunction() {
        return new Function<Integer, String>() {
            @Override
            public String apply(@Nullable Integer input) {
                if (input == 1) {
                    Participant participant = participantList.get(0);
                    if (participant.getParticipantType().equals("ad_hoc")) {
                        return participant.getDisplayName();
                    } else {
                        return participant.getName();
                    }
                } else if (input > 2) {
                    Participant participant = participantList.get(input - 2);
                    if (participant.getParticipantType().equals("ad_hoc")) {
                        return participant.getDisplayName();
                    } else {
                        return participant.getName();
                    }
                }
//                return participantList.get(input).getName();
                throw new RuntimeException("Invalid index");
            }
        };
    }

    private void handleServerMessage(ServerMessage message) {
        MessageType messageType = message.getMessageType();
        Log.i(TAG, "MessageType:" + messageType);
        //TODO - message validation

        switch (messageType) {
            case GET_PARTICIPANTS:
                handleGetParticipants(message);
                break;
//            case GET_CONFERENCE_CONFIG:
            case GET_CONFERENCE_INFO:
//                handleGetConferenceConfig(message);
                handleGetConferenceInfo(message);
                break;
            case END_CONFERENCE:
                handleEndConference();
                break;
        }
    }


    @SuppressWarnings("unchecked")
    private void handleGetParticipants(ServerMessage message) {
        List<Participant> participants =
                (List<Participant>) message.getProperty(ServerMessageConstants.KEY_AP_PARTICIPANTS);
        participantList.clear();

        participantList.addAll(participants);
        Collections.sort(participantList, new Comparator<Participant>() {
            @Override
            public int compare(Participant p1, Participant p2) {
                if(p1.isLecturerGroup()) {
                    return -1;
                } else if(p2.isLecturerGroup()){
                    return 1;
                }

                String p1ValueToCompare = p1.getParticipantType().equals("ad_hoc") ?
                                                                p1.getDisplayName() : p1.getName();
                String p2ValueToCompare = p2.getParticipantType().equals("ad_hoc") ?
                                                                p2.getDisplayName() : p2.getName();

                return p1ValueToCompare.compareTo(p2ValueToCompare);
            }
        });


        participantsAdapter.notifyDataSetChanged();

//        Toast.makeText(getApplicationContext(),
//                        "Received participants list",
//                        Toast.LENGTH_SHORT).show();
//                        Toast.LENGTH_LONG).show();
    }

//    private void handleGetConferenceConfig(ServerMessage message) {
//        Toast.makeText(getApplicationContext(),
//                "Received conference config",
//                Toast.LENGTH_LONG).show();
//
//        conf = (Conference) message.getProperty(ServerMessageConstants.KEY_AP_CONFERENCE);
//        EventBus.getDefault().postSticky(new ConferenceEvent(conf));
//        selectedFocusType = conf.getFocusType();
//        participantsAdapter.notifyDataSetChanged();
//    }

    private void handleGetConferenceInfo(ServerMessage message) {
        Toast.makeText(getApplicationContext(),
                "Received conference info",
                Toast.LENGTH_LONG).show();

        conf = (ConferenceInfo) message.getProperty(ServerMessageConstants.KEY_AP_CONFERENCE);
        startTimeTextView.setText("Start time: " + conf.getActiveStartTime());
        endTimeTextView.setText("End time: " + conf.getActiveEndTime());

        endConferenceButton.setVisibility(View.VISIBLE);
//        EventBus.getDefault().postSticky(new ConferenceEvent(conf));
//        selectedFocusType = conf.getFocusType();
//        participantsAdapter.notifyDataSetChanged();

    }

    private void handleEndConference() {
        Toast.makeText(getApplicationContext(), "Conference has finished", Toast.LENGTH_SHORT).show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                finish();
            }
        }, 2000);
    }

    private void toggleSelection(int position) {
        participantsAdapter.toggleSelection(position);
        int count = participantsAdapter.getSelectedItemCount();

        if (count == 0) {
            mActionMode.finish();
        } else {
            mActionMode.setTitle(String.valueOf(count));
            mActionMode.invalidate();
        }
    }

    private void signOut() {
        finish();
        stopService(new Intent(ParticipantsActivity.this, WebSocketService.class));
        startLoginActivity();
    }

    private void startLoginActivity() {
        Intent intent = new Intent(ParticipantsActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    private Participant getParticipantForIndex(int index) {
        if (index == 1) {
            return participantList.get(0);
        } else if (index > 2) {
            return participantList.get(index - 2);
        }

        throw new RuntimeException("Invalid index");
    }

    private class GetConferenceInfoTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
//            requestExecutor.requestConferenceConfig(conference);
            requestExecutor.requestConferenceInfo(conference);
            int i = 0;
            while (i < 4) {
                if (conf != null) {
                    break;
                }

                try {
                    Thread.sleep(1000);
                    i++;
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            requestExecutor.requestParticipantsList(conference);
        }
    }

    private class MessengerServiceConnectTask extends AsyncTask<Void, Void, Messenger> {

        @Override
        protected Messenger doInBackground(Void... params) {
            doBindService();

            //TODO consider using sth beter - e.g. Lock
            int i = 0;
            while (i < 4) {
                if (mIsBound) {
                    break;
                }

                try {
                    Thread.sleep(1000);
                    i++;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            return mService;
        }

        @Override
        protected void onPostExecute(Messenger messenger) {
            requestExecutor = new RequestExecutor(username, messenger);
            sendInitialConferenceConfigToServer();
            new GetConferenceInfoTask().execute();
        }
    }

    private class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case WebSocketService.MSG_RECEIVE_FROM_SERVER:
                    handleServerMessage((ServerMessage) msg.obj);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    private void doBindService() {
        bindService(new Intent(ParticipantsActivity.this, WebSocketService.class),
                mConnection,
                Context.BIND_AUTO_CREATE);
    }

    private void doUnbindService() {
        if (mIsBound) {
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null, WebSocketService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                        /*Nothing we can do.*/
                    Log.e(TAG, e.getMessage());
                }
            }

            unbindService(mConnection);
            mIsBound = false;
            Log.i(TAG, "Unbinding service");
        }
    }
}
