package pl.edu.agh.placzkie.ctp_supervisor.websocket;

import com.google.common.base.Preconditions;
import com.google.gson.annotations.SerializedName;

import java.util.Collections;
import java.util.Map;

/**
 * Created by Leszek Placzkiewicz on 2016-08-14.
 */
public class ServerMessage {

    @SerializedName("messageType")
    private final MessageType messageType;

    @SerializedName("additionalProperties")
    private final Map<String, Object> additionalProperties;


    public ServerMessage(MessageType messageType, Map<String, Object> additionalProperties) {
        this.messageType = Preconditions.checkNotNull(messageType);
        this.additionalProperties = Preconditions.checkNotNull(additionalProperties);
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public Map<String, Object> getAdditionalProperties() {
        return Collections.unmodifiableMap(additionalProperties);
    }

    public Object getProperty(String name) {
        return additionalProperties.get(name);
    }
}
