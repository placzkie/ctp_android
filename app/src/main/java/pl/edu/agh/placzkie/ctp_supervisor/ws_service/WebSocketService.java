package pl.edu.agh.placzkie.ctp_supervisor.ws_service;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Log;

import pl.edu.agh.placzkie.ctp_supervisor.R;
import pl.edu.agh.placzkie.ctp_supervisor.websocket.OutgoingMessage;
import pl.edu.agh.placzkie.ctp_supervisor.websocket.ServerMessage;
import pl.edu.agh.placzkie.ctp_supervisor.websocket.WebSocketConnector;

/**
 * Created by Leszek Placzkiewicz on 2016-08-20.
 */
public class WebSocketService extends Service {

    private static final String TAG = WebSocketService.class.getName();

    public static final int MSG_REGISTER_CLIENT = 1;

    public static final int MSG_UNREGISTER_CLIENT = 2;

    public static final int MSG_SEND_TO_SERVER = 3;

    public static final int MSG_RECEIVE_FROM_SERVER = 4;

    public static final int MSG_SIGN_IN = 5;

    public static final int MSG_CONN_DONE = 6;


    private final Messenger mMessenger = new Messenger(new IncomingHandler());

    private final WebSocketConnector webSocketConnector;

    private Messenger mClient;


    public WebSocketService() {
        this.webSocketConnector = new WebSocketConnector(this);
    }

    public void sendToClient(ServerMessage serverMessage) {
        try {
            mClient.send(Message.obtain(null, MSG_RECEIVE_FROM_SERVER, serverMessage));
        } catch (RemoteException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
//        return START_FLAG_REDELIVERY - TODO - check
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "Created " + getClass().getName());
    }

    @Override
    public void onDestroy() {
        webSocketConnector.disconnect();
        Log.d(TAG, "Destroyed " + getClass().getName());
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "Bound " + getClass().getName());
        return mMessenger.getBinder();
    }


    private final class IncomingHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    mClient = msg.replyTo;
                    break;
                case MSG_UNREGISTER_CLIENT:
                    mClient = null;
                    break;
                case MSG_SIGN_IN:
                    String credentials = (String) msg.obj;
                    int colonIndex = credentials.indexOf(":");
                    String username = credentials.substring(0, colonIndex);
                    String password = credentials.substring(colonIndex + 1);


                    String ip = extractSharedProperty(
                            obtainStringResource(R.string.pref_key_ip_address),
                            obtainStringResource(R.string.pref_default_ip_address));

                    String port = extractSharedProperty(
                            obtainStringResource(R.string.pref_key_port),
                            obtainStringResource(R.string.pref_default_port));


                    boolean connected = webSocketConnector.connect(ip, port, username, password);
                    try {
                        msg.replyTo.send(Message.obtain(null, MSG_SIGN_IN, connected));
                        msg.replyTo.send(Message.obtain(null, MSG_CONN_DONE, webSocketConnector.wasConnectDone()));
                    } catch (RemoteException e) {
                        Log.e(TAG, e.getMessage());
                    }
                    break;
                case MSG_SEND_TO_SERVER:
                    OutgoingMessage outgoingMessage = (OutgoingMessage) msg.obj;
                    webSocketConnector.send(outgoingMessage);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }

        private String obtainStringResource(int resourceId) {
            return getResources().getText(resourceId).toString();
        }

        private String extractSharedProperty(String propertyName, String defaultValue) {
            SharedPreferences sp =
                    PreferenceManager.getDefaultSharedPreferences(WebSocketService.this);
            return sp.getString(propertyName, defaultValue);
        }
    }
}