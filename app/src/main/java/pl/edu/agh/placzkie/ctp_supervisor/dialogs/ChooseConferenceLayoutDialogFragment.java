package pl.edu.agh.placzkie.ctp_supervisor.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import org.greenrobot.eventbus.EventBus;

import pl.edu.agh.placzkie.ctp_supervisor.R;
import pl.edu.agh.placzkie.ctp_supervisor.model.ConferenceEvent;

/**
 * Created by Leszek Placzkiewicz on 2016-09-07.
 */
public class ChooseConferenceLayoutDialogFragment extends DialogFragment {

    private int selectedMode;

    private ChooseConferenceLayoutDialogListener mListener;

//    private int currentLayout;

//    private String layoutSource;

    private String cpLayout;

    public int getSelectedMode() {
        return selectedMode;
    }

    public String getCpLayout() {
//        int familyIndex = selectedMode + 1;
        int familyIndex = 1;
        if(selectedMode == 0) {
            familyIndex = 1;
        } else if(selectedMode == 1) {
            familyIndex = 2;
        } else if (selectedMode == 2) {
            familyIndex = 4;
        }

        return "family" + familyIndex;
//        switch (selectedMode) {
//            case 0:
//                return "conferenceCustom";
//            case 1:
//                return "family1";
//            case 2:
//                return "family2";
//            case 3:
//                return "family3";
//            case 4:
//                return "family4";
//            case 5:
//                return "family5";
//            default:
//                throw new RuntimeException("Invalid index");
//        }
    }
//    public String getCpLayout() {
////        TODO - maybe handle also: default, layoutx
//        switch (selectedMode) {
//            case 0:
//                return "conferenceCustom";
//            case 1:
//                return "family1";
//            case 2:
//                return "family2";
//            case 3:
//                return "family3";
//            case 4:
//                return "family4";
//            case 5:
//                return "family5";
//            default:
//                throw new RuntimeException("Invalid index");
//        }
//    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (ChooseConferenceLayoutDialogListener) activity;
        } catch (ClassCastException ex) {
            throw new ClassCastException(activity.toString()
                    + " must implement ChooseConferenceLayoutDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setCurrentLayout();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.dialog_title_choose_conference_layout)
                .setSingleChoiceItems(R.array.conference_layout_modes,
                        findItemIndex(cpLayout),
//                                        findItemIndex(currentLayout, layoutSource),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                selectedMode = which;
        //                        mListener.onClick(ChooseConferenceLayoutDialogFragment.this);
                            }
                        })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onClick(ChooseConferenceLayoutDialogFragment.this);
                    }
                });

        return builder.create();
    }

    private int findItemIndex(String cpLayout) {
        int familyIndex = Integer.parseInt(cpLayout.substring("family".length()));
//        switch (cpLayout) {
//            case "conferenceCustom":
//                return 0;
//            case "familyx":
//                return currentLayout;
//            case "participantCustom:": //TODO -  is it possible??
//            default :
//                throw new RuntimeException("Unknown layoutSource");
//        }
        if(familyIndex == 1) {
            return 0;
        } else if(familyIndex == 2) {
            return 1;
        }else if(familyIndex == 4) {
            return 2;
        }
        throw new AssertionError("Invalid layout family index");
    }

//    private int findItemIndex(int currentLayout, String layoutSource) {
//        switch (layoutSource) {
//            case "conferenceCustom":
//                return 0;
//            case "familyx":
//                return currentLayout;
////            case "participantCustom:": //TODO -  is it possible??
//            default :
//                throw new RuntimeException("Unknown layoutSource");
//        }
//    }

    private void setCurrentLayout() {
        ConferenceEvent conferenceEvent =
                EventBus.getDefault().getStickyEvent(ConferenceEvent.class);
//        currentLayout = conferenceEvent.getConference().getCurrentLayout();
//        layoutSource = conferenceEvent.getConference().getLayoutSource();
        cpLayout = conferenceEvent.getConference().getCpLayout();
    }

    public interface ChooseConferenceLayoutDialogListener {
        void onClick(ChooseConferenceLayoutDialogFragment dialog);
    }
}
