package pl.edu.agh.placzkie.ctp_supervisor.activities.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import com.google.common.collect.ImmutableMap;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;
import java.util.Map;

import pl.edu.agh.placzkie.ctp_supervisor.R;
import pl.edu.agh.placzkie.ctp_supervisor.model.Conference;
import pl.edu.agh.placzkie.ctp_supervisor.model.ConferenceEvent;
import pl.edu.agh.placzkie.ctp_supervisor.model.FocusType;
import pl.edu.agh.placzkie.ctp_supervisor.model.Participant;

/**
 * Created by Leszek Placzkiewicz on 2016-07-17.
 */
public class ParticipantsAdapter extends
        SelectableAdapter {

    private static final String TAG = ParticipantsAdapter.class.getSimpleName();

    private static final int TYPE_SECTION = 0;

    private static final int TYPE_ITEM = 1;


    private ParticipantViewHolder.ClickListener clickListener;

    private SectionViewHolder.ClickListener sectionsClickListener;

    private List<Participant> participantsList;

    private Conference conference;

    private int participantNameColorActive;

    private int participantNameColorInactive;

    private final Map<Integer, Integer> sectionIndexToSectionNameResourceId;

    public ParticipantsAdapter(List<Participant> participantsList,
                               ParticipantViewHolder.ClickListener clickListener,
                               SectionViewHolder.ClickListener sectionsClickListener) {
        super();

        this.clickListener = clickListener;
        this.sectionsClickListener = sectionsClickListener;
        this.participantsList = participantsList;

        EventBus.getDefault().register(this);

        sectionIndexToSectionNameResourceId = initializeSectionIndicesMap();
    }

    private Map<Integer, Integer> initializeSectionIndicesMap() {
        return ImmutableMap.of(0, R.string.section_lecturer,
                                2, R.string.section_schools);
    }

    @Subscribe
    public void onConferenceEvent(ConferenceEvent conferenceEvent) {
        conference = conferenceEvent.getConference();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        final int layout = viewType == TYPE_ACTIVE ?
//                R.layout.participant_list_row_active : R.layout.participant_list_row;

//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.participant_list_row, parent, false);
//                .inflate(layout, parent, false);

        participantNameColorActive = ContextCompat.getColor(parent.getContext(),
                                                            R.color.participantNameColorActiveSpeaker);
        participantNameColorInactive = ContextCompat.getColor(parent.getContext(),
                                                            R.color.participantNameColorInactiveSpeaker);

//        return new ParticipantViewHolder(itemView, clickListener);

        if (viewType == TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.participant_list_row, parent, false);
            return new ParticipantViewHolder(itemView, clickListener);
        } else if (viewType == TYPE_SECTION) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.section_row, parent, false);
            return new SectionViewHolder(itemView, sectionsClickListener);
        }

        throw new RuntimeException("There is no type that matches: " + viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//        Participant participant = participantsList.get(position);
//
//        holder.participantName.setTextColor(participant.isActiveSpeaker() ?
//                                    participantNameColorActive : participantNameColorInactive);
//        holder.participantName.setText(participant.getName());
//        holder.microphoneSwitch.setChecked(!participant.isAudioMuted());
//        holder.microphoneSwitch.setOnCheckedChangeListener(holder);
//        holder.lecturer.setVisibility(participant.isLecturer() ? View.VISIBLE : View.INVISIBLE);
//        holder.importantParticipant.setVisibility(
//                                    participant.isImportant() ? View.VISIBLE : View.INVISIBLE);
//        holder.layoutSelectedParticipant.setVisibility(
//                        conference != null &&
//                        conference.getFocusType() == FocusType.PARTICIPANT &&
//                        participant.getName().equals(conference.getFocusParticipant()) ?
//                                                                    View.VISIBLE : View.INVISIBLE);
//
//        holder.selectedOverlay.setVisibility(isSelected(position) ? View.VISIBLE : View.INVISIBLE);

        if (holder instanceof ParticipantViewHolder) {
            ParticipantViewHolder participantHolder = (ParticipantViewHolder) holder;
            Participant participant = participantsList.get(getParticipantIndex(position));

            participantHolder.participantName.setTextColor(participant.isActiveSpeaker() ?
                    participantNameColorActive : participantNameColorInactive);
            participantHolder.participantName.setText(
                    participant.getParticipantType().equals("ad_hoc") ?
                            participant.getDisplayName() : participant.getName());
            participantHolder.microphoneSwitch.setChecked(!participant.isAudioMuted());
            participantHolder.microphoneSwitch.setOnCheckedChangeListener(participantHolder);
            participantHolder.lecturer.setVisibility(participant.isLecturer() ? View.VISIBLE : View.INVISIBLE);
//            participantHolder.importantParticipant.setVisibility(
//                    participant.isImportant() ? View.VISIBLE : View.INVISIBLE);
            conference = EventBus.getDefault().getStickyEvent(ConferenceEvent.class).getConference();
            participantHolder.layoutSelectedParticipant.setVisibility(
                    !conference.isAutomaticLectureModeEnabled() &&
                    conference != null &&
                            conference.getFocusType() == FocusType.PARTICIPANT &&
                            obtainParticipantTypeDependentName(participant)
                                                        .equals(conference.getFocusParticipant()) ?
                            View.VISIBLE : View.INVISIBLE);

//            participantHolder.selectedOverlay.setVisibility(isSelected(getParticipantIndex(position)) ? View.VISIBLE : View.INVISIBLE);
            participantHolder.selectedOverlay.setVisibility(isSelected(position) ? View.VISIBLE : View.INVISIBLE);


        } else if (holder instanceof SectionViewHolder) {
            SectionViewHolder sectionViewHolder = (SectionViewHolder) holder;
            if(participantsList.isEmpty() && position==1) {
                sectionViewHolder.sectionName.setText(sectionIndexToSectionNameResourceId.get(position+1));
            } else {
                sectionViewHolder.sectionName.setText(sectionIndexToSectionNameResourceId.get(position));
            }

            if(position == 0){
//                sectionViewHolder.microphonesSwitch.setVisibility(View.INVISIBLE);
                sectionViewHolder.onAllMicrophonesButton.setVisibility(View.INVISIBLE);
                sectionViewHolder.offAllMicrophonesButton.setVisibility(View.INVISIBLE);
            } else {
//                sectionViewHolder.microphonesSwitch.setVisibility(View.VISIBLE);
                sectionViewHolder.onAllMicrophonesButton.setVisibility(View.VISIBLE);
                sectionViewHolder.offAllMicrophonesButton.setVisibility(View.VISIBLE);
            }

            sectionViewHolder.onAllMicrophonesButton.setOnClickListener(sectionViewHolder);
            sectionViewHolder.offAllMicrophonesButton.setOnClickListener(sectionViewHolder);
        }
    }

    private String obtainParticipantTypeDependentName(Participant participant) {
        return participant.getParticipantType().equals("ad_hoc") ?
                                            participant.getDisplayName() : participant.getName();
    }

    @Override
    public int getItemViewType(int position) {
//        final Participant participant = participantsList.get(position);
//        return participant.isActiveSpeaker() ? TYPE_ACTIVE : TYPE_INACTIVE;

        if (isSectionPosition(position))
            return TYPE_SECTION;

        return TYPE_ITEM;
    }

    private boolean isSectionPosition(int position) {
        if (participantsList.isEmpty() && position == 1) {
            position += 1;
        }

        return sectionIndexToSectionNameResourceId.containsKey(position);
    }

    @Override
    public int getItemCount() {
//        return participantsList.size();
        //participants + two sections
        return participantsList.size() + 2;
    }

    private int getParticipantIndex(int position) {
        if (position == 1) {
            return 0;
        } else if (position > 2) {
            return position - 2;
        }

        throw new RuntimeException("Illegal position");
    }

    public List<Participant> getParticipantsList() {
        return participantsList;
    }


    public static class ParticipantViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener,
            View.OnLongClickListener,
            CompoundButton.OnCheckedChangeListener {

        private static final String TAG = ParticipantViewHolder.class.getSimpleName();

        private TextView participantName;

        private Switch microphoneSwitch;

        private ImageButton lecturer;

//        private ImageButton importantParticipant;

        private ImageButton layoutSelectedParticipant;

        private View selectedOverlay;

        private ClickListener clickListener;


        public ParticipantViewHolder(View view, final ClickListener listener) {
            super(view);

            participantName = (TextView) view.findViewById(R.id.participant_name);

            lecturer = (ImageButton) view.findViewById(R.id.lecturer);

//            importantParticipant = (ImageButton) view.findViewById(R.id.important_participant);

            layoutSelectedParticipant =
                                (ImageButton) view.findViewById(R.id.layout_selected_participant);

            microphoneSwitch = (Switch) view.findViewById(R.id.microphoneSwitch);

            selectedOverlay = view.findViewById(R.id.selected_overlay);

            clickListener = listener;

            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (clickListener != null && buttonView.isPressed()) {
                clickListener.onSwitchClicked(getLayoutPosition(), isChecked);
            }
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) {
                clickListener.onItemClicked(getLayoutPosition());
            }
        }

        @Override
        public boolean onLongClick(View view) {
            if (clickListener != null) {
                return clickListener.onItemLongClicked(getLayoutPosition());
            }

            return false;
        }

        public interface ClickListener {
            void onItemClicked(int position);

            boolean onItemLongClicked(int position);

            void onSwitchClicked(int position, boolean isChecked);
        }
    }

    public static class SectionViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{

        private final TextView sectionName;

//        private final Switch microphonesSwitch;

        private final ImageButton onAllMicrophonesButton;

        private final ImageButton offAllMicrophonesButton;

        private final ClickListener clickListener;

        public SectionViewHolder(View view, ClickListener clickListener) {
            super(view);

            sectionName = (TextView) view.findViewById(R.id.section_name);

//            microphonesSwitch = (Switch) view.findViewById(R.id.microphones_switch);
            onAllMicrophonesButton = (ImageButton) view.findViewById(R.id.on_all_microphones);
            offAllMicrophonesButton = (ImageButton) view.findViewById(R.id.off_all_microphones);

            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.on_all_microphones:
                    clickListener.enableAllMicrophonesClicked();
                    break;
                case R.id.off_all_microphones:
                    clickListener.disableAllMicrophonesClicked();
                    break;
            }
        }

        public interface ClickListener {
            void enableAllMicrophonesClicked();

            void disableAllMicrophonesClicked();
        }
    }
}
