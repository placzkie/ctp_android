package pl.edu.agh.placzkie.ctp_supervisor.websocket;

/**
 * Created by Leszek Placzkiewicz on 2016-08-13.
 */
public class AdditionalMessagePropertiesConstants {

    public static final String KEY_USERNAME = "username";

    public static final String KEY_CONFERENCE_NAME = "conferenceName";

    public static final String KEY_MESSAGE_TEXT = "message_text";//TODO maybe change to messageText on server side

    public static final String KEY_PARTICIPANT_NAME = "participantName";

    public static final String KEY_FOCUS_TYPE = "focusType";

    public static final String KEY_CP_LAYOUT = "cpLayout";

    public static final String KEY_AUTOMATIC_LECTURE_MODE = "automaticLectureMode";
}
