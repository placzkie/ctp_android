package pl.edu.agh.placzkie.ctp_supervisor.websocket;

import android.util.Log;

import com.google.common.base.Preconditions;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import pl.edu.agh.placzkie.ctp_supervisor.ws_service.WebSocketService;

/**
 * Created by Leszek Placzkiewicz on 2016-07-12.
 */
public class WebSocketConnector {

    private static final String TAG = WebSocketConnector.class.getName();

    private static final String WS_PREFIX = "ws://";

    private static final String HOST_PORT_DELIMITER = ":";

    private static final String PARAMETERS_START_INDICATOR = "?";

    private static final String PARAMETER_ASSIGNMENT_OPERATOR = "=";

    private static final String PARAMETERS_DELIMITER = "&";

    private static final String USERNAME_PARAMETER = "username";

    private static final String PASSWORD_PARAMETER = "password";


    private CTPWebSocketClient webSocketClient;

    private final WebSocketService webSocketService;

    private final MessageParser messageParser;

    private boolean connectDone;


    public WebSocketConnector(WebSocketService webSocketService) {
        this.webSocketService = Preconditions.checkNotNull(webSocketService);
        this.messageParser = new MessageParser();
    }

    public boolean connect(String host, String port, String username, String password) {
        Preconditions.checkNotNull(host);
        Preconditions.checkNotNull(port);
        Preconditions.checkNotNull(username);
        Preconditions.checkNotNull(password);

        URI uri = convertToURI(prepareStringAddress(host, port, username, password));

        webSocketClient = new CTPWebSocketClient(uri);

        boolean res = false;
        FutureTask<Boolean> future = new FutureTask<>(new Callable<Boolean>() {
                    public Boolean call() {
                        try {
                            return webSocketClient.connectBlocking();
                        } catch (InterruptedException e) {
                            Log.e(TAG, e.getMessage());
                        }
                        return false;
                    }});
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(future);
        try {
            res = future.get(3, TimeUnit.SECONDS);
            connectDone = future.isDone();

        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            // If timeout exception occurs res will be false - ok
        }
        return res;
    }

    public boolean wasConnectDone() {
        return connectDone;
    }

    public void disconnect() {
        webSocketClient.getConnection().close();
    }

    public void send(OutgoingMessage outgoingMessage) {
        String message = messageParser.toJson(outgoingMessage);
        webSocketClient.send(message);
    }

    private String prepareStringAddress(String host, String port,
                                        String username, String password) {
        return WS_PREFIX +
                host + HOST_PORT_DELIMITER + port +
                PARAMETERS_START_INDICATOR +
                USERNAME_PARAMETER + PARAMETER_ASSIGNMENT_OPERATOR + username +
                PARAMETERS_DELIMITER +
                PASSWORD_PARAMETER + PARAMETER_ASSIGNMENT_OPERATOR + password;
    }

    private URI convertToURI(String address) {
        URI uri = null;
        try {
            uri = new URI(address);
        } catch (URISyntaxException e) {
            Log.e(TAG, e.getMessage());
        }

        return uri;
    }

    public class CTPWebSocketClient extends WebSocketClient {

        public CTPWebSocketClient(URI serverURI) {
            super(serverURI);
        }

        @Override
        public void onOpen(ServerHandshake serverHandshake) {
            Log.d(TAG, "Connection to server is now opened.");
        }

        @Override
        public void onMessage(String message) {
            Log.i(TAG, "Received : " + message);
            ServerMessage serverMessage = messageParser.fromJson(message);
            webSocketService.sendToClient(serverMessage);
        }

        @Override
        public void onClose(int code, String reason, boolean remote) {
            Log.d(TAG, "Connection to server is now closed. Reason : " + reason);
        }

        @Override
        public void onError(Exception ex) {
            Log.e(TAG, "Error " + ex.getMessage());
        }
    }
}
