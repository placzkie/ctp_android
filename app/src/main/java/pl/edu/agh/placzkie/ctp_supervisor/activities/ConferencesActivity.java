package pl.edu.agh.placzkie.ctp_supervisor.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pl.edu.agh.placzkie.ctp_supervisor.R;
import pl.edu.agh.placzkie.ctp_supervisor.activities.adapter.ConferencesAdapter;
import pl.edu.agh.placzkie.ctp_supervisor.activities.additions.DividerItemDecoration;
import pl.edu.agh.placzkie.ctp_supervisor.activities.additions.IntentConstants;
import pl.edu.agh.placzkie.ctp_supervisor.websocket.MessageType;
import pl.edu.agh.placzkie.ctp_supervisor.websocket.RequestExecutor;
import pl.edu.agh.placzkie.ctp_supervisor.websocket.ServerMessage;
import pl.edu.agh.placzkie.ctp_supervisor.websocket.ServerMessageConstants;
import pl.edu.agh.placzkie.ctp_supervisor.ws_service.WebSocketService;

/**
 * Created by Leszek Placzkiewicz on 2016-10-10.
 */
public class ConferencesActivity extends AppCompatActivity
        implements ConferencesAdapter.ConferenceViewHolder.ConferenceClickListener {
    private static final String TAG = ConferencesActivity.class.getName();

    private RecyclerView recyclerView;

    private List<String> conferencesList = new ArrayList<>();

    private ConferencesAdapter conferencesAdapter;

    private RequestExecutor requestExecutor;

    private String username;

    //WebSocketService variables
    private volatile boolean mIsBound;

    private Messenger mService = null;

    private final Messenger mMessenger = new Messenger(new IncomingHandler());

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = new Messenger(service);

            try {
                Message msg = Message.obtain(null, WebSocketService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);

            } catch (RemoteException e) {
                    /*Nothing we can do*/
                Log.e(TAG, e.getMessage());
            }

            Log.i(TAG, "Web socket service connected.");
            mIsBound = true;
        }

        public void onServiceDisconnected(ComponentName className) {
            mService = null;
            Log.i(TAG, "Web socket service disconnected.");
        }
    };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conferences);

        Intent intent = getIntent();
        username = intent.getStringExtra(IntentConstants.USERNAME);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.conferencesAppBar);
        setSupportActionBar(myToolbar);

        conferencesAdapter = new ConferencesAdapter(conferencesList, this);

        recyclerView = (RecyclerView) findViewById(R.id.conferences_recycler_view);
        RecyclerView.LayoutManager mLayoutManager =
                new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(
                new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(conferencesAdapter);

        new MessengerServiceConnectTask().execute((Void) null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.conferences_app_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sign_out:
                signOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        doUnbindService();
        super.onDestroy();
    }

    private void handleServerMessage(ServerMessage message) {
        MessageType messageType = message.getMessageType();
        Log.i(TAG, "MessageType:" + messageType);
        //TODO - message validation

        switch (messageType) {
            case GET_CONFERENCES:
                handleGetConferences(message);
                break;
        }
    }

    @SuppressWarnings("unchecked")
    private void handleGetConferences(ServerMessage message) {
        List<String> conferences =
                (List<String>) message.getProperty(ServerMessageConstants.KEY_AP_CONFERENCES);
        conferencesList.clear();
        conferencesList.addAll(conferences);
        conferencesAdapter.notifyDataSetChanged();
        Log.i(TAG, "Conferences = " + conferences);
    }

    private void signOut() {
        finish();
        stopService(new Intent(ConferencesActivity.this, WebSocketService.class));
        startLoginActivity();
    }

    private void startLoginActivity() {
        Intent intent = new Intent(ConferencesActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void onItemClicked(int position) {
        String selectedConference = conferencesList.get(position);
        Toast.makeText(getApplicationContext(),
                selectedConference + " chosen",
                Toast.LENGTH_LONG).show();

        doUnbindService();
        startParticipantsActivity(selectedConference);
    }

    private void startParticipantsActivity(String selectedConference) {
        Intent intent = new Intent(ConferencesActivity.this, ParticipantsActivity.class);
        intent.putExtra(IntentConstants.USERNAME, username);
        intent.putExtra(IntentConstants.CONFERENCE, selectedConference);
        startActivity(intent);
    }

    private class MessengerServiceConnectTask extends AsyncTask<Void, Void, Messenger> {

        @Override
        protected Messenger doInBackground(Void... params) {
            doBindService();

            //TODO consider using sth beter - e.g. Lock
            int i = 0;
            while (i < 4) {
                if (mIsBound) {
                    break;
                }

                try {
                    Thread.sleep(1000);
                    i++;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            return mService;
        }

        @Override
        protected void onPostExecute(Messenger messenger) {
            requestExecutor = new RequestExecutor(username, messenger);
            requestExecutor.requestConferenceList();
        }
    }

    private class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case WebSocketService.MSG_RECEIVE_FROM_SERVER:
                    handleServerMessage((ServerMessage) msg.obj);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    private void doBindService() {
        bindService(new Intent(ConferencesActivity.this, WebSocketService.class),
                mConnection,
                Context.BIND_AUTO_CREATE);
    }

    private void doUnbindService() {
        if (mIsBound) {
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null, WebSocketService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                        /*Nothing we can do.*/
                    Log.e(TAG, e.getMessage());
                }
            }

            unbindService(mConnection);
            mIsBound = false;
            Log.i(TAG, "Unbinding service");
        }
    }
}
