package pl.edu.agh.placzkie.ctp_supervisor.websocket;

/**
 * Created by Leszek Placzkiewicz on 2016-09-15.
 */
public class ServerMessageConstants {

    public static final String KEY_MESSAGE_TYPE = "messageType";

    public static final String KEY_USERNAME = "username";

    public static final String KEY_CONFERENCE_NAME = "conferenceName";

    public static final String KEY_ADDITIONAL_PROPERTIES = "additionalProperties";

    public static final String KEY_AP_PARTICIPANTS = "participants";

    public static final String KEY_AP_CONFERENCES = "conferences";

    public static final String KEY_AP_PARTICIPANTS_NAME = "name";

    public static final String KEY_AP_PARTICIPANTS_AUDIO_MUTED = "audioMuted";

    public static final String KEY_AP_PARTICIPANTS_LECTURER = "lecturer";

    public static final String KEY_AP_PARTICIPANTS_ACTIVE_SPEAKER = "activeSpeaker";

    public static final String KEY_AP_PARTICIPANTS_IMPORTANT = "important";

    public static final String KEY_AP_PARTICIPANTS_DISPLAY_NAME = "displayName";

    public static final String KEY_AP_PARTICIPANTS_PARTICIPANT_TYPE = "participantType";

    public static final String KEY_AP_PARTICIPANTS_LECTURER_GROUP = "lecturerGroup";

    public static final String KEY_AP_CONFERENCE = "conference";

    public static final String KEY_AP_CONFERENCE_CONFERENCE_NAME = "conferenceName";

    //    public static final String KEY_AP_CONFERENCE_FOCUS_TYPE = "focusType";
//
//    public static final String KEY_AP_CONFERENCE_FOCUS_PARTICIPANT_NAME = "focusParticipantName";
//
//    public static final String KEY_AP_CONFERENCE_CURRENT_LAYOUT = "currentLayout";
//
//    public static final String KEY_AP_CONFERENCE_LAYOUT_SOURCE = "layoutSource";
//
    public static final String KEY_AP_CONFERENCE_ACTIVE_START_TIME = "activeStartTime";

    public static final String KEY_AP_CONFERENCE_ACTIVE_END_TIME = "activeEndTime";
}
