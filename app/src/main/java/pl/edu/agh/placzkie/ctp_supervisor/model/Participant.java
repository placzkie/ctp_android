package pl.edu.agh.placzkie.ctp_supervisor.model;

import com.google.common.base.Preconditions;

/**
 * Created by Leszek Placzkiewicz on 2016-07-17.
 */
public class Participant {

    private final String name;

    private final String displayName;

    private boolean audioMuted;

    private boolean important;

    private boolean activeSpeaker;

    private boolean lecturer;

    private String participantType;

    private boolean lecturerGroup;

    //TODO - builder
    public Participant(String name,
                       boolean audioMuted,
                       String displayName,
                       boolean important,
                       boolean activeSpeaker,
                       boolean lecturer,
                       String participantType,
                       boolean lecturerGroup) {
        this.name = Preconditions.checkNotNull(name);
        this.audioMuted = audioMuted;
        this.displayName = displayName;
        this.important = important;
        this.activeSpeaker = activeSpeaker;
        this.lecturer = lecturer;
        this.participantType = participantType;
        this.lecturerGroup = lecturerGroup;
    }

    public String getName() {
        return name;
    }

    public boolean isAudioMuted() {
        return audioMuted;
    }

    public void setAudioMuted(boolean audioMuted) {
        this.audioMuted = audioMuted;
    }

    public String getDisplayName() {
        return displayName;
    }

    public boolean isImportant() {
        return important;
    }

    public boolean isActiveSpeaker() {
        return activeSpeaker;
    }

    public boolean isLecturer() {
        return lecturer;
    }

    public String getParticipantType() {
        return participantType;
    }

    public boolean isLecturerGroup() {
        return lecturerGroup;
    }
}
